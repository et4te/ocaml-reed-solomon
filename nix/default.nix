{ pkgs, stdenv, opam2nix, fetchFromGitHub, makeRustPlatform }:

let
  src = fetchFromGitHub {
    owner = "mozilla";
    repo = "nixpkgs-mozilla";
    rev = "bf862af36145f5c1e566f93fe099e36bddeac0c8";
    sha256 = "0g6a0jss350k1ki7srhwkx62x93gxi2zxjdlii0m4bn5i7pxfcn4";
  };
in
with import "${src.out}/rust-overlay.nix" pkgs pkgs;

let
  rustPlatform = makeRustPlatform {
    cargo = latest.rustChannels.stable.rust;
    rustc = latest.rustChannels.stable.rust;
  };

  reed_solomon_rs = rustPlatform.buildRustPackage rec {
    name = "reed_solomon_rs-${version}";
    version = "0.1";
    src = ../.;
    cargoSha256 = "0sjjj9z1dhilhpc8pq4154czrb79z9cm044jvn75kxcjv6v5l2m5";
    meta = with stdenv.lib; {
      description = "A stub for a Rust reed solomon implementation.";
      homepage = https://gitlab.com/et4te/ocaml-reed-solomon;
      license = licenses.mit;
    };
  };
in

with stdenv.lib;

stdenv.mkDerivation {
    name = "reed_solomon";
    src = ../.;
    buildInputs = [reed_solomon_rs] ++ opam2nix.build {
      specs = opam2nix.toSpecs [ "dune" "ocamlbuild" "ocamlfind" ];
      ocamlAttr = "ocaml";
      ocamlVersion = "4.06.1";
    };
    buildPhase = ''
      mkdir -p target/release
      cp ${reed_solomon_rs}/lib/libreed_solomon_stubs.a target/release/libreed_solomon_stubs.a
      cp ${reed_solomon_rs}/lib/libreed_solomon_stubs.so target/release/libreed_solomon_stubs.so
      dune build
    '';
    installPhase = ''
      dune runtest test
      cp -R _build $out/lib/reed_solomon
    '';
}
