
pub mod gf256;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_polynomial_operations() {
        // Scaling
        let s = gf256::FieldElement::new(2u8);
        let p = gf256::Polynomial::from_bytes(vec![1,2,3,4]);
        let p_scaled = p.scale(s);
        assert_eq!(vec![2,4,6,8], p_scaled.as_bytes());

        // Addition
        let p = gf256::Polynomial::from_bytes(vec![1,2,3,4]);
        let q = gf256::Polynomial::from_bytes(vec![2,4,6,8]);
        let r = p + q;
        assert_eq!(vec![3,6,5,12], r.as_bytes());

        // Multiplication
        let p = gf256::Polynomial::from_bytes(vec![1,2,3,4]);
        let q = gf256::Polynomial::from_bytes(vec![2,4,6,8]);
        let r = (&p) * (&q);
        assert_eq!(vec![2,0,8,0,10,0,32], r.as_bytes());
        
        let p = gf256::Polynomial::from_bytes(vec![1,2,3,4,1,2,3]);
        let q = gf256::Polynomial::from_bytes(vec![2,4,6,8,9,10]);
        let (r1, r2) = gf256::Polynomial::extended_synthetic_division(p, q);
        assert_eq!(vec![1, 6], r1.as_bytes());
        assert_eq!(vec![29, 24, 56, 62, 63], r2.as_bytes());

        // Evaluation
        let p = gf256::Polynomial::from_bytes(vec![1,2,3,4]);
        let p_evaluation = p.eval(5);
        assert_eq!(124, p_evaluation);

        // Generator Polynomial
        let g = gf256::Polynomial::generate(10);
        assert_eq!(g.as_bytes(), vec![1, 216, 194, 159, 111, 199, 94, 95, 113, 157, 193]);
    }

    #[test]
    fn test_reed_solomon_operations() {
        // Encoding
        let encoded = gf256::encode(vec![1,2,3,4,5], 10);
        let result = vec![1, 2, 3, 4, 5, 201, 229, 50, 108, 140, 243, 72, 192, 6, 130];
        assert_eq!(encoded.clone(), result);

        // Syndromes
        let syn = gf256::syndromes(encoded.clone(), 10);
        assert!(gf256::check_syndromes(encoded.clone(), 10));

        // Correcting known errors
        let eloc = gf256::find_errata_locator(encoded.clone());
        let result = vec![206, 166, 110, 191, 141, 156, 25, 70, 85, 133, 143, 209, 247, 97, 217, 1];
        assert_eq!(eloc.clone(), result);

        let eeval = gf256::find_error_evaluator(syn.clone(), eloc.clone(), 10);
        let result = vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        assert_eq!(eeval.clone(), result);

        let correct_msg = vec![1, 2, 3, 4, 5, 201, 229, 50, 108, 140, 243, 72, 192, 6, 130];
        let mut msg = encoded.clone();
        msg[0] = 0;
        let syn = gf256::syndromes(msg.clone(), 10);
        let expected_syn = vec![0, 1, 19, 24, 181, 93, 94, 107, 67, 129, 102];
        assert_eq!(syn.clone(), expected_syn);

        let corrected = gf256::correct_errata(msg.clone(), syn.clone(), vec![0u8]);
        assert_eq!(corrected.clone(), correct_msg.clone());

        // Finding and correcting errors
        let mut msg = corrected.clone();
        msg[0] = 6u8;
        msg[9] = 7u8;
        let syn = gf256::syndromes(msg.clone(), 10);
        assert_eq!(syn.clone(), vec![0, 140, 201, 203, 129, 138, 7, 193, 76, 74, 35]);
        let err_loc = gf256::find_error_locator(gf256::Polynomial::from_bytes(syn.clone()), 10, None, 0).unwrap();
        let mut err_loc = err_loc.as_bytes();
        assert_eq!(err_loc, vec![90, 51, 1]);
        err_loc.reverse();
        let err_loc = gf256::Polynomial::from_bytes(err_loc);
        let pos = gf256::find_errors(err_loc, msg.len() as u8).unwrap();
        assert_eq!(pos, vec![9, 0]);
        let msg = gf256::correct_errata(msg, syn, pos);
        assert_eq!(msg, correct_msg);
    }    
}
