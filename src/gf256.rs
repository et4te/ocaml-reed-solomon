
use std::ops::{Add, Sub, Mul, Div, Index, BitXor};
use std::sync::{Once, ONCE_INIT};
use std::cmp::max;
use std::collections::VecDeque;
use std::iter::FromIterator;

pub struct GaloisField256 {
    pub exp_table: [u8; 512],
    pub log_table: [u8; 256],
}

static INIT: Once = ONCE_INIT;
static mut GF256: GaloisField256 = GaloisField256 {
    exp_table: [0u8; 512],
    log_table: [0u8; 256],
};

pub fn get_gf256() -> &'static GaloisField256 {
    INIT.call_once(|| {
        let gf256 = unsafe { &mut GF256 };
        let mut x = 1;
        for i in 0..255 {
            gf256.exp_table[i] = x;
            gf256.log_table[x as usize] = i as u8;
            x = russian_peasant_mul(x, 2, 0x11d, 256, true);
        }
        for i in 255..512 {
            gf256.exp_table[i] = gf256.exp_table[i - 255];
        }
    });
    return unsafe { &GF256 };
}

fn russian_peasant_mul(x: u8, y: u8, primitive_polynomial: u16, full_field_char: u16, carryless: bool) -> u8 {
    let mut r: u8 = 0;
    let mut x: u16 = x as u16;
    let mut y: u8 = y;
    while y > 0 {
        if (y & 1) > 0 {
            r = if carryless {
                r ^ (x as u8)
            } else {
                r + (x as u8)
            };
        }
        y = y >> 1;
        x = x << 1;
        if primitive_polynomial > 0 && ((x & full_field_char) == full_field_char) {
            x = x ^ primitive_polynomial;
        }
    }
    return r
}

pub fn encode(m: Vec<u8>, nsym: usize) -> Vec<u8> {
    let gen = Polynomial::generate(nsym);
    let gen_bytes = gen.as_bytes();
    let suffix = vec![0u8; gen_bytes.len() - 1];
    let p = Polynomial::from_bytes([m.clone(), suffix].concat());
    let (_, remainder) = Polynomial::extended_synthetic_division(p, gen);
    [m, remainder.as_bytes()].concat()
}

pub fn syndromes(m: Vec<u8>, nsym: usize) -> Vec<u8> {
    let p = Polynomial::from_bytes(m);
    let mut syn = vec![0u8; nsym];
    let two = FieldElement::new(2u8);
    for i in 0..nsym {
        syn[i] = (&p).eval(two.pow(i as i16));
    }
    [vec![0u8], syn].concat()
}

pub fn check_syndromes(m: Vec<u8>, nsym: usize) -> bool {
    syndromes(m, nsym).iter().max().cloned() == Some(0u8)
}

pub fn find_errata_locator(epos: Vec<u8>) -> Vec<u8> {
    let one_p = Polynomial::from_bytes(vec![1u8]);
    let two = FieldElement::new(2u8);
    let mut eloc = Polynomial::from_bytes(vec![1u8]);
    for i in epos {
        let p = Polynomial::from_bytes(vec![two.pow(i as i16), 0u8]);
        let q = one_p.clone() + p;
        eloc = &eloc * &q;
    }
    return eloc.as_bytes()
}

pub fn find_error_evaluator(syn: Vec<u8>, eloc: Vec<u8>, nsym: u8) -> Vec<u8> {
    let p = &Polynomial::from_bytes(syn) * &Polynomial::from_bytes(eloc);
    let q = Polynomial::from_bytes([vec![1], vec![0u8; nsym as usize + 1]].concat());
    let (_, remainder) = Polynomial::extended_synthetic_division(p, q);
    remainder.as_bytes()
}

pub fn correct_errata(msg: Vec<u8>, syn: Vec<u8>, epos: Vec<u8>) -> Vec<u8> {
    let coef_pos: Vec<usize> = epos.iter().map(|p| msg.len() - 1 - (*p as usize)).collect();
    let eloc = find_errata_locator(coef_pos.iter().map(|p| *p as u8).collect());
    let mut revsyn = syn.clone();
    revsyn.reverse();
    let eeval = find_error_evaluator(revsyn, eloc.clone(), (eloc.len() - 1) as u8);

    let mut xpoly: Vec<u8> = vec![];
    for i in 0..coef_pos.len() {
        let l: i16 = 255 - (coef_pos[i] as i16);
        xpoly.push(FieldElement::new(2u8).pow(-l));
    }

    let one = FieldElement::new(1u8);
    let mut emagp = vec![0u8; msg.len()];
    let xpolylen = xpoly.len();
    for (i, xi) in xpoly.iter().enumerate() {
        let xi = FieldElement::new(*xi);
        let xi_inv = FieldElement::new(xi.inv());

        let mut eloc_prime_tmp = vec![];
        for j in 0..xpolylen {
            if j != i {
                eloc_prime_tmp.push(one - (xi_inv * FieldElement::new(xpoly[j])));
            }
        }

        let mut eloc_prime = one.clone();
        for coef in eloc_prime_tmp {
            eloc_prime = eloc_prime * coef;
        }

        let y = Polynomial::from_bytes(eeval.clone()).eval(xi_inv.value);
        let y = FieldElement::new(xi.pow(1i16)) * FieldElement::new(y);

        let mag = y / eloc_prime;
        emagp[epos[i] as usize] = mag.value;
    }
    
    let p = Polynomial::from_bytes(msg);
    let q = Polynomial::from_bytes(emagp);
    (p + q).as_bytes()
}

pub fn find_error_locator(syn: Polynomial, nsym: usize, erase_loc: Option<Polynomial>, erase_count: usize) -> Result<Polynomial, ()> {
    // Initialise polynomials
    let mut err_loc: Polynomial = match erase_loc.clone() {
        Some(erase_loc) =>
            erase_loc.clone(),
        None =>
            Polynomial::from_bytes(vec![1u8]),
    };
    let mut old_loc: Polynomial = err_loc.clone();

    // Fix the syndrome shifting
    let mut syn_shift = 0;
    if syn.length() > nsym {
        syn_shift = syn.length() - nsym;
    }

    let lim = nsym - erase_count;

    for i in 0..lim {
        let k = match erase_loc {
            Some(_) =>
                erase_count + i + syn_shift,
            None =>
                i + syn_shift,
        };

        // Compute the discrepancy delta
        let mut delta = syn[k];
        for j in 1..err_loc.length() {
            delta = delta ^ (err_loc[err_loc.length() - (j+1)] * syn[k - j]);
        }

        // Shift polynomials to compute the next degree
        let tmp_loc: Vec<u8> = [old_loc.clone().as_bytes(), vec![0u8]].concat();
        old_loc = Polynomial::from_bytes(tmp_loc);

        // Iteratively estimate the errata locator and evaluator polynomials
        if !delta.is_zero() {
            if old_loc.length() > err_loc.length() {
                let new_loc = old_loc.clone().scale(delta);
                old_loc = err_loc.scale(delta.inverse());
                err_loc = new_loc;
            }
            err_loc = err_loc + old_loc.clone().scale(delta);
        }
    }

    let mut err_loc_dq = VecDeque::from_iter(err_loc.as_bytes());
    while err_loc_dq.len() > 0 && err_loc_dq[0] == 0 {
        err_loc_dq.pop_front();
    }
    let errs = err_loc_dq.len() - 1;
    if (errs - erase_count) * 2 + erase_count > nsym {
        return Err(()); // Error::TooManyErrors
    }

    let p = Polynomial::from_bytes(err_loc_dq.iter().cloned().collect());
    Ok(p)
}

pub fn find_errors(err_loc: Polynomial, msg_len: u8) -> Result<Vec<u8>, ()> {
    let two = FieldElement::new(2u8);
    let nerrs = err_loc.length() - 1;
    let mut err_pos = vec![];
    for i in 0..msg_len {
        if err_loc.eval(two.pow(i as i16)) == 0u8 {
            err_pos.push(msg_len - 1 - i);
        }
    }
    if err_pos.len() != nerrs {
        return Err(()) // Too many or few errors
    }
    Ok(err_pos)
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Polynomial {
    pub value: Vec<FieldElement>
}

impl Polynomial {

    pub fn generate(nsym: usize) -> Polynomial {
        let two = FieldElement::new(2u8);
        let mut g = Polynomial::from_bytes(vec![1u8]);
        for i in 0..nsym {
            g = (&g) * (&Polynomial::from_bytes(vec![1u8, two.pow(i as i16)]));
        }
        g
    }

    pub fn length(&self) -> usize {
        self.value.len()
    }

    pub fn from_bytes(bytes: Vec<u8>) -> Polynomial {
        let mut poly: Vec<FieldElement> = vec![];
        for byte in bytes {
            poly.push(FieldElement::new(byte));
        }
        Polynomial { value: poly }
    }

    pub fn as_bytes(&self) -> Vec<u8> {
        let mut bytes: Vec<u8> = vec![];
        for fe in self.value.clone() {
            bytes.push(fe.value);
        }
        bytes
    }

    pub fn scale(self, x: FieldElement) -> Polynomial {
        let mut r: Vec<FieldElement> = vec![FieldElement::new(0u8); self.value.len()];
        for i in 0..self.value.len() {
            r[i] = self[i] * x;
        }
        Polynomial { value: r }
    }

    pub fn eval(&self, x: u8) -> u8 {
        let x = FieldElement::new(x);
        let mut y: FieldElement = self[0];
        for i in 1..self.length() {
            y = (y * x) + self[i];
        }
        y.value
    }

    pub fn extended_synthetic_division(dividend: Polynomial, divisor: Polynomial) -> (Polynomial, Polynomial) {
        let mut output: Vec<FieldElement> = dividend.value.clone();
        for i in 0..(dividend.length() - (divisor.length() - 1)) {
            let coef = output[i];
            if !coef.is_zero() {
                for j in 1..divisor.length() {
                    if !divisor[j].is_zero() {
                        output[i + j] = output[i + j] ^ (divisor[j] * coef);
                    }
                }
            }
        }
        let i = divisor.length() - 1 as usize;
        let n = dividend.length() - i;
        ( Polynomial { value: output.iter().take(n).cloned().collect() },
          Polynomial { value: output.iter().skip(n).cloned().collect() } )
    }
}

impl Index<usize> for Polynomial {
    type Output = FieldElement;

    fn index(&self, i: usize) -> &FieldElement {
        &self.value[i]
    }
}

impl Add<Polynomial> for Polynomial {
    type Output = Polynomial;

    #[inline]
    fn add(self, q: Polynomial) -> Polynomial {
        let mut r: Vec<FieldElement> = vec![FieldElement::new(0u8); max(self.length(), q.length())];
        for i in 0..self.length() {
            let j = i + r.len() - self.length();
            r[j] = self[i];
        }
        for i in 0..q.length() {
            let j = i + r.len() - q.length();
            r[j] = r[j] ^ q[i];
        }
        Polynomial { value: r }
    }
}

impl<'a> Mul<&'a Polynomial> for &'a Polynomial {
    type Output = Polynomial;

    #[inline]
    fn mul(self, q: &Polynomial) -> Polynomial {
        let mut r = vec![FieldElement::new(0u8); self.length() + q.length() - 1];
        for j in 0..q.length() {
            for i in 0..self.length() {
                r[i+j] = r[i+j] ^ (self[i] * (&q)[j]);
            }
        }
        Polynomial { value: r }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct FieldElement {
    pub value: u8
}

impl FieldElement {
    pub fn new(value: u8) -> FieldElement {
        FieldElement { value: value }
    }

    pub fn zero() -> FieldElement {
        FieldElement { value: 0u8 }
    }

    pub fn is_zero(self) -> bool {
        self.value == 0 
    }

    pub fn pow(self, power: i16) -> u8 {
        let gf256 = get_gf256();
        let p = (gf256.log_table[self.value as usize] as i16) * power;
        let i = if p < 0 {
            255 + p
        } else {
            p % 255
        };
        gf256.exp_table[i as usize]
    }

    pub fn inv(self) -> u8 {
        let gf256 = get_gf256();
        let i = 255 - gf256.log_table[self.value as usize];
        gf256.exp_table[i as usize]
    }

    pub fn inverse(self) -> FieldElement {
        let gf256 = get_gf256();
        let i = 255 - gf256.log_table[self.value as usize];
        FieldElement::new(gf256.exp_table[i as usize])
    }
}

impl Add<FieldElement> for FieldElement {
    type Output = FieldElement;

    #[inline]
    fn add(self, rhs: FieldElement) -> FieldElement {
        FieldElement { value: self.value ^ rhs.value }
    }
}

impl Sub<FieldElement> for FieldElement {
    type Output = FieldElement;

    #[inline]
    fn sub(self, rhs: FieldElement) -> FieldElement {
        FieldElement { value: self.value ^ rhs.value }
    }
}

impl Mul<FieldElement> for FieldElement {
    type Output = FieldElement;

    #[inline]
    fn mul(self, rhs: FieldElement) -> FieldElement {
        let gf256 = get_gf256();
        if self.value == 0 || rhs.value == 0 {
            return FieldElement::zero();
        }
        let i = (gf256.log_table[self.value as usize] as usize) + (gf256.log_table[rhs.value as usize] as usize);
        return FieldElement::new(gf256.exp_table[i]);
    }
}

impl Div<FieldElement> for FieldElement {
    type Output = FieldElement;

    #[inline]
    fn div(self, rhs: FieldElement) -> FieldElement {
        let gf256 = get_gf256();
        if rhs.value == 0 {
            panic!("FieldElement: DivideByZero");
        }
        if self.value == 0 {
            return FieldElement::zero();
        }
        let i = (gf256.log_table[self.value as usize] as usize + 255) - ((gf256.log_table[rhs.value as usize] as usize) % 255);
        return FieldElement::new(gf256.exp_table[i as usize]);
    }
}

impl BitXor<FieldElement> for FieldElement {
    type Output = FieldElement;

    #[inline]
    fn bitxor(self, rhs: FieldElement) -> FieldElement {
        FieldElement { value: self.value ^ rhs.value }
    }
}
